﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        [HttpGet]
        public ActionResult<IEnumerable<String>> Get()
        {
            return new string[] { "value", "Hello" };
        }

        [HttpGet("{id}")]
        public ActionResult<String> Get(int id)
        {
            return "Hello" + id;
        }


        [HttpPost]
        public ActionResult<Users> Post([FromBody] Users users)
        {
            return users;
        }

        [HttpPut("{id}")]
        public ActionResult<string> Put(int id, [FromBody] Users users)
        {
            return $"แก้ไข  Users ID : {id} แล้วเรียบร้อย";
        }

        [HttpDelete("{id}")]
        public ActionResult<string> Delete(int id)
        {
            return $"ลบ User ID: {id} แล้วครับ";
        }
    }
}
