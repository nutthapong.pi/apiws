﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Dbcontext;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyCoffeeStore : ControllerBase
    {
        public readonly EmptyClass _context;
        private readonly IConfiguration _config;

        public MyCoffeeStore(EmptyClass context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        private string GenerateJWTToken(string userGuid)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(1);

            var token = new JwtSecurityToken(
                issuer: _config["JwtIssuer"],
                audience: _config["JwtAudience"],
                claims: new List<Claim> { new Claim("user_guid",userGuid) },
                expires: expires,
                signingCredentials:creds
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [HttpGet("GetProductUser")]
        public ActionResult<List<Users>> GetProductUser()
        {
            return _context.Users.ToList();
        }

        [HttpGet("GetProduct")]
        public ActionResult<List<Product>> GetProduct()
        {
            return _context.Products.ToList();
        }

        [HttpPost("CreateNewProduct")]
        public async Task<ActionResult<Product>> CreateNewProduct([FromBody] Product prod)
        {
            await _context.Products.AddAsync(prod);
            await _context.SaveChangesAsync();
            return prod;
        }


        [HttpPost("UpdateProduct/{id}")]
        public async Task<ActionResult<Product>> UpdateProduct(int id, [FromQuery] string productName, [FromQuery] decimal price)
        {
            Product result = null;
            bool isBadRequest = false;

            try
            {
                if (string.IsNullOrEmpty(productName))
                    throw new Exception("กรุณากรอกชื่อสินค้า");

                result = await _context.Products.Where(w => w.ProductId == id).FirstOrDefaultAsync();

                if (result == null)
                    throw new Exception($"ไม่พบรหัสสินค้า {id}");

                result.ProductName = productName;

                _context.Update(result);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                isBadRequest = true;
                result = new Product { ProductName = ex.Message };
            }
            if (isBadRequest)
                return BadRequest(result);

            return Ok(result);
        }

        //[HttpDelete("DeleteProduct/{id}")]
        //public async Task<ActionResult<ResponseMessage>> DeleteProduct(int id)
        //{
        //    try
        //    {
        //        var prod = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();
        //        if(prod == null)
        //        {
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = $"ไม่พบรหัสสินค้า {id}" });
        //        }
        //        _context.Remove(prod);

        //        await _context.SaveChangesAsync();

        //        return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = $"ลบข้อมูลสินค้ารหัส {id} เรียบร้อย" });
        //    }
        //    catch(Exception ex)
        //    {
        //        return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message =ex.Message});
        //    }
        //}









        [HttpGet("GetProductDetail")]
        public ActionResult<List<ProductDetail>> GetProductDetail()
        {
             _context.Products.Where(x => x.IsDeleted == false).ToList();
            return _context.ProductDetails.ToList();
        }

        [HttpPost("CreateNewProductDetail")]
        public async Task<ActionResult<ProductDetail>> CreateNewProductDetail([FromBody] ProductDetail prod)
        {
            var res = await _context.Products.Where(x => x.ProductId == prod.ProductId).FirstOrDefaultAsync();
            if (res == null)
            {
                return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = $"ไม่มีข้อมูล ProductID : {prod.ProductId}" });
            }
            else
            {
                await _context.ProductDetails.AddAsync(prod);
                await _context.SaveChangesAsync();
                return Ok(prod);
            }
        }


        [HttpPost("UpdateProductDetail/{id}")]
        public async Task<ActionResult<ProductDetail>> UpdateProductDetail(int id,[FromQuery] string productDetailName ,[FromQuery] decimal productDetailPrice)
        {
            ProductDetail res = null;
            bool isBad = false;

            try
            {
                if (string.IsNullOrEmpty(productDetailName))
                                throw new Exception("กรุณากรอก productDetailName");

                            res = await _context.ProductDetails.Where(x => x.ProductDetailID == id).FirstOrDefaultAsync();

                            if(res == null)
                            {
                                throw new Exception($"ไม่พบรหัสสินค้า {id}");
                            }
                            res.ProductDetailName = productDetailName;

                            res.ProductDetailPrice = productDetailPrice;

                            _context.Update(res);
                            await _context.SaveChangesAsync();

            }
            catch(Exception ex)
            {
                isBad = true;
                res = new ProductDetail { ProductDetailName = ex.Message };
            }
            if (isBad)
                return BadRequest(res);

            return Ok(res);

        }

        [HttpDelete("DeleteProduct/{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            Product res = null;
            bool isBad = false;

            try
            {

                res = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();

                if (res == null)
                {
                    throw new Exception($"ไม่พบรหัสสินค้า {id}");
                }
                res.IsDeleted = true;

                _context.Update(res);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                isBad = true;
                res = new Product { ProductName = ex.Message };
            }
            if (isBad)
                return BadRequest(res);

            return Ok(res);

        }


        [HttpGet("GetProductTransaction/{id}")]
        public ActionResult<List<Transaction>> GetProductTransaction(int id)
        {
            _context.Users.ToList();
            Transaction res = null;

            try
            {
                res = _context.Transactions.Where(x => x.Transactionid == id).FirstOrDefault();

                if (res == null)
                {
                    throw new Exception($"ไม่พบรหัส TransactionId : {id}");
                }

                return Ok(res);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("GetTransactionDetail/{id}")]
        public ActionResult<List<TransactionDetail>> GetTransactionDetail(int id)
        {
            _context.Users.ToList();
            _context.Products.ToList();
            _context.Transactions.ToList();
            _context.ProductDetails.ToList();
            List<TransactionDetail> res = null;
            try
            {
                res = _context.TransactionDetails.Where(x => x.Transactionid == id).ToList();

                if (res == null)
                {
                    throw new Exception($"ไม่พบรหัส TransactionId : {id}");
                }
                return Ok(res);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            //return _context.TransactionDetails.ToList();
        }














        //[AllowAnonymous]
        //[HttpGet("GetNewTokenForExistingEmployee")]
        //public async Task<ActionResult<string>> GetNewTokenForExistingEmployee([FromQuery]string userName , [FromQuery] string password)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(userName))
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "กรอก User name ด้วยจ้า" });
        //        if (string.IsNullOrEmpty(password))
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "กรอก Password ด้วยจ้า" });

        //        var _user = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(userName) && x.password.Equals(password));

        //        if(_user == null)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "Username หรือ Password ไม่ถูกต้อง" });

        //        string token = GenerateJWTToken(_user.guid.ToString());
        //        return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = token });

        //    }
        //    catch(Exception ex)
        //    {
        //        return StatusCode((int)System.Net.HttpStatusCode.InternalServerError,
        //            new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
        //    }
        //}
        //[AllowAnonymous]
        //[HttpPost("RegisterEmployee")]
        //public async Task<ActionResult<ResponseMessage>> RegisterEmployee([FromBody] Employee user)
        //{
        //    try
        //    {
        //        var chkUser = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(user.userName));
        //        if (chkUser != null)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "User name ซ้ำ" });
        //        Users _user = new Users
        //        {
        //            guid = Guid.NewGuid(),
        //            userName = user.userName.Trim(),
        //            password = user.password.Trim(),
        //            firsName = user.firsName.Trim(),
        //            lastname = user.lastname.Trim(),
        //            email = user.email.Trim(),
        //            mobile = user.mobile.Trim()
        //        };
        //        await _context.Users.AddAsync(_user);
        //        await _context.SaveChangesAsync();

        //        string token = GenerateJWTToken(_user.guid.ToString());
        //        return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = token });
        //    }
        //    catch(Exception ex)
        //    {
        //        return StatusCode((int)System.Net.HttpStatusCode.InternalServerError,
        //            new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
        //    }
        //}

        //[Myauth]
        //[HttpPost("CreateTransaction")]
        //public async Task<ActionResult<ResponseMessage>> CreateTransaction([FromBody] Transaction transaction)
        //{
        //    try
        //    {
        //        if (transaction.ProductId < 1)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "ใส่ productid ด้วยครับ" });

        //        if (transaction.ReceivedMoney < 1)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "ลูกค้าลืมจ่ายตังหรอเปล่า" });

        //        var _product = await _context.Products.Where(x => x.ProductId == transaction.ProductId).FirstOrDefaultAsync();
        //        if (_product == null)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "ใส่ productid ผิด!!!" });

        //        decimal charge = transaction.ReceivedMoney - _product.Price;
        //        if (charge < 0)
        //            return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "ลุกค้าจ่ายเงินไม่พอ" });

        //        var employee = await _context.Users.Where(x => x.guid == new Guid(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "user_guid").Value)).FirstOrDefaultAsync();

        //        var _trans = new Transaction
        //        {
        //            ProductId = _product.ProductId,
        //            ReceivedMoney = transaction.ReceivedMoney,
        //            UserId = employee.id,
        //            TransactionDate = DateTime.Now,
        //            Charge = charge
        //        };

        //        await _context.AddAsync(_trans);
        //        await _context.SaveChangesAsync();

        //        string result = $"ขอบคุณที่ใช้บริการ เลขที่ใบเสร็จ : {_trans.Transactionid} เงินทอน : {charge} บาท";

        //        return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = result });

        //    }
        //    catch(Exception ex)
        //    {
        //        return StatusCode((int)System.Net.HttpStatusCode.InternalServerError,
        //            new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
        //    }
        //}




        public class ResponseMessage
        {
            public int statusCode { get; set; }

            public string message { get; set; }
        }
    }
}
