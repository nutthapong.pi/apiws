﻿using System;
using System.Linq;
using System.Threading.Tasks;
using API.Dbcontext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using API.Models;
using static API.Controllers.MyCoffeeStore;

namespace API.Controllers
{
    public class Myauth : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var dbcontext = context.HttpContext.RequestServices.GetService(typeof(EmptyClass)) as EmptyClass;
                var config = context.HttpContext.RequestServices.GetService(typeof(IConfiguration)) as IConfiguration;
                var user = context.HttpContext.User;

                if (!user.Identity.IsAuthenticated)
                    throw new Exception();

                string user_guid = user.Claims.First(c => c.Type == "user_guid").Value;
                if (dbcontext.Users.FirstOrDefault(x => x.guid == new Guid(user_guid)) == null)
                    throw new Exception();
            }
            catch
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }


        
    }
}
