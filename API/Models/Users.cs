﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class Users
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        public Guid guid { get; set; }

        [Required]
        public string userName { get; set; }

        [Required]
        public string password { get; set; }

        [Required]
        public string firsName { get; set; }

        [Required]
        public string lastname { get; set; }

        public string email { get; set; }

        [Required]
        public string mobile { get; set; }
    }
}
