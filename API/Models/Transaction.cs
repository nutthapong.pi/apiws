﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    public class Transaction
    {
        [Key]
        public int Transactionid { get; set; }

        [ForeignKey(nameof(UserId))]
        public int UserId { get; set; }

        public decimal ReceivedMoney { get; set; }

        public decimal TotalTransactionDetaiPrice { get; set; }

        public decimal Charge { get; set; }

        public DateTime TransactionDate { get; set; }



        public Users User { get; set; }

    }
}
