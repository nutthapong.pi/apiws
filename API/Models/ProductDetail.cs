﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    public class ProductDetail
    {
        [Key]
        public int ProductDetailID { get; set; }

        [ForeignKey(nameof(ProductId))]
        public int ProductId { get; set; }

        public string ProductDetailName { get; set; }

        public decimal ProductDetailPrice { get; set; }

        public Product Product { get;set; }

    }
}
