﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    public class TransactionDetail
    {
        [Key]
        public int TransactionDetailId { get; set; }

        [ForeignKey(nameof(Transactionid))]
        public int Transactionid { get; set; }

        [ForeignKey(nameof(ProductDetailID))]
        public int ProductDetailID { get; set; }

        public int Quantity { get; set; }

        public decimal TotalPrie { get; set; }

        
        public Transaction Transaction { get; set; }

 
        public ProductDetail ProductDetail { get; set; }
    }
}
