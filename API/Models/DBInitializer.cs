﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Dbcontext;
using static API.Startup;

namespace API.Models
{
    public class DBInitializer : IDbInitializer
    {
        private readonly EmptyClass _context;

        public DBInitializer(EmptyClass context)
        {
            _context = context; 
        }

        public async Task DataSeeding()
        {
            try
            {
                await _context.AddRangeAsync(new List<TransactionDetail>
                {
                    new TransactionDetail{ Transactionid = 1, ProductDetailID = 2 , Quantity = 3, TotalPrie = 200 },
                    new TransactionDetail{ Transactionid = 1, ProductDetailID = 3 , Quantity = 30, TotalPrie = 200 }
                });

                await _context.AddRangeAsync(new List<Transaction>
                {
                    new Transaction{UserId = 1, ReceivedMoney = 10, TotalTransactionDetaiPrice = 120 , Charge  = 110 , TransactionDate= DateTime.Now },
                    new Transaction{UserId = 1, ReceivedMoney = 20, TotalTransactionDetaiPrice = 220 , Charge  = 210 , TransactionDate= DateTime.Now }
                });

                await _context.AddRangeAsync(new List<ProductDetail>
                {
                    new ProductDetail{ProductDetailName = "โค้ก", ProductId = 1, ProductDetailPrice = 12},
                    new ProductDetail{ProductDetailName = "น้ำเปล่า", ProductId = 1, ProductDetailPrice = 10},
                    new ProductDetail{ProductDetailName = "ขนม", ProductId = 2, ProductDetailPrice = 22},

                });


                await _context.AddRangeAsync(new List<Product>
                {
                    new Product { ProductName = "น้ำ" , IsDeleted = false},
                    new Product { ProductName = "ขนม", IsDeleted = false},
                    new Product { ProductName = "ของใช้" , IsDeleted = false},
                    
                });

                await _context.Users.AddAsync(new Users
                {
                    userName = "admin",
                    password = "admin",
                    firsName = "Admin",
                    lastname = "NaJa",
                    guid = Guid.NewGuid(),
                    email = "test1@hotmail.com",
                    mobile = "0879785767"
                });

                await _context.SaveChangesAsync();
            }
            catch
            {

            }
        }

    }
}
