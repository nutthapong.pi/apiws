﻿using System;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Dbcontext
{
    public class EmptyClass : DbContext
    {
        public EmptyClass(DbContextOptions<EmptyClass> options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionDetail> TransactionDetails { get; set; }
        public DbSet<Users> Users { get; set; }

    }
}
